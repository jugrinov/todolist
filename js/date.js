function leftUntil ( dateUntil ) {
    let now = new Date();
    let target = new Date( dateUntil );

    let timeLeft = new Date( target.getTime() - now.getTime() );

    return {
        years: timeLeft.getUTCFullYear() - 1970,
        months: timeLeft.getUTCMonth(),
        days: timeLeft.getUTCDate() - 1,
        hours: timeLeft.getUTCHours(),
        minutes: timeLeft.getUTCMinutes(),
        seconds: timeLeft.getUTCSeconds(),
        miliseconds: timeLeft.getUTCMilliseconds()
    }
}

function createDayDropdown () {
    const dropdown = document.createElement('select');

    for(let i=1; i<=31; i++) {
        const option = document.createElement('option');
        option.value = i;
        option.textContent= i;
        dropdown.appendChild(option);
    }

    document.getElementById('dateContainer').appendChild(dropdown);
    
}

function createMonthDropdown () {
    const dropdown = document.createElement('select');

    monthNames = [
        'Jan','Feb','Mar','Apr','Maj','Jun','Jul','Avg','Sep','Okt','Nov','Dec'
    ];

    for(let i=0; i<=11; i++) {
        const option = document.createElement('option');
        option.value = i;
        option.textContent= monthNames[i];
        dropdown.appendChild(option);
    }

    document.getElementById('monthContainer').appendChild(dropdown);
    
}

function createYearDropdown() {
    const dropdown = document.createElement('select');
    const now = new Date();

    for(let i = now.getFullYear(); i <= now.getFullYear() + 5; i++) {
        const option = document.createElement('option');
        option.value = i;
        option.textContent= i;
        dropdown.appendChild(option);
    }

    document.getElementById('yearContainer').appendChild(dropdown);
    
}

function dateSignal () {
    const day = document.getElementById('dateContainer').children[0].value;
    const month = document.getElementById('monthContainer').children[0].value;
    const year = document.getElementById('yearContainer').children[0].value;
    const deadLine = new Date(year, month, day);
    const elems = {
        action: document.getElementById('action_select'),
        title: document.querySelector('.input_title_desc'),
        date: deadLine,  //akonapisem toLocaleDateString dobicu bez dana u nedelji i samo sa brojkama
        desc: document.querySelector('.input_description')
    }

    let now = new Date();
    let target = new Date(elems.date);

    let timeLeft = new Date( target.getTime() - now.getTime() );

   untilDeadline = {
       years: timeLeft.getUTCFullYear() - 1970,  
       months: timeLeft.getUTCMonth(),
       days: timeLeft.getUTCDate() - 1,
       hours: timeLeft.getUTCHours(),
       minutes: timeLeft.getUTCMinutes(),
       seconds: timeLeft.getUTCSeconds(),
   }

   if(untilDeadline.days <= 2 && untilDeadline.days > 1) {
    div2.classList.add('yellow-signal');
    div3.classList.add('yellow-signal-date');
} else if(untilDeadline.days <= 1) {
    div2.classList.add('red-signal');
    div3.classList.add('red-signal-date');
}
}
