// createDayDropdown ();
// createMonthDropdown ();
// createYearDropdown();

const itemsPerPage = 8;

const filter = {
    _limit: itemsPerPage,
    _page: 1,
    _sort: 'date',
    _order: 'asc'   //asc ili desc
}
let dialog;

$(document).ready(function(){

    
    //kalendar iz UI paketa
    $('#deadline').datepicker({
        dateFormat: "yy-mm-dd",
        minDate: '+1d'
    });

    loadData({}, false);
    loadData(filter, 1);

    $('#actionFilter').change(function(){

        let filter = {}
        if($(this).val() !== '') {
            filter.action = $(this).val()
        }
    
        loadData(filter, 1); 
    
    });
    
    $('#orderBy').change(() => {
        filter._sort = $(this).val();
        loadData(filter, 1);
    });

    $(function(){
        $( "#itemDetailes" ).dialog({
            autoOpen: false,
            show: {
              effect: "blind",
              duration: 1000
            },
            hide: {
              effect: "explode",
              duration: 1000
            }
        });
        
    });

    // $('#timeFilter').change(function() {
    //     const now = new Date();
    //     const twoDays = 1000 * 60 * 60 * 24 * 2;
    //     const twoWeeks = twoDays * 2;
    //     if($(this).val() === 'pased') {
    //         filter.date = function(data) {
    //             for(id=1; id<=data.length; id++) {
    //                 if(data.date < now) {
    //                     return data
    //                 }
    //             }
    //         }
    //     }
    //     loadData(filter, 1);
    // });GRRRRRRRRRR!!!!!!!!
        
    
});

function loadData(filter, page) {
    //Clear data from html DOM
    const ul = document.querySelector('.cont_princ_lists > ul');
    ul.innerHTML = '';

    if(page) {
        filter._page = page;
        filter._limit = itemsPerPage;
    }

    //Load already saved data
    $.ajax({
        url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
        data: filter,
        method: 'GET',
        dataType: 'json',
        beforeSend: function() {
            //prikazuje loader
            $('#loader').removeClass('hidden')
        }
    }).done(function(data){
        if(data.length > 0){
            if (page) {
                for(i=0; i<data.length; i++){
                    //console.log('element' + i, data[i]);
                    data[i].date = new Date(data[i].date);
                    createListItem( data[i] );
                    $('.cont_princ_lists > ul > li').click(function(){
                        $( "#itemDetailes" ).dialog('open');
                    });    
                }
                $('.active').removeClass('active');
                $('#pageLink'+page).addClass('active');
            } else {
                printPagination(data.length);
            }
        }
    }).fail(function() {
        alert('Failed')
    }).always(function() {
        //uklanja loader
        $('#loader').addClass('hidden ')
    });
}

//Ukupan broj / itemPerPage        
function printPagination(itemCount) {
    const pageCount = Math.ceil(itemCount / itemsPerPage);
    const paginationContainer = document.getElementById('pagination');

    for(i = 1; i <= pageCount; i++) {
        //kreiranje novog link elementa
        let link = document.createElement('span');
        //dodavanje vrednosti u element
        link.textContent = i;
        //dodavanje klik iventa
        link.addEventListener('click', function(e) {
            //alert('pozivam stranicu '+ e.target.textContent);
            loadData(filter, e.target.textContent);
        });

        link.setAttribute('id', 'pageLink' + i); //svaki span da ima svoj id
        if( i == 1 ) {
            link.classList.add('active');
        }
        paginationContainer.appendChild(link);
    }
}   

function add_new() {
    // selektovati element  u kojem se nalazi forma
    const forma = document.querySelector('.cont_crear_new');

    // Dodavanje ili oduzimanje css klase koja cini da forma postane vidljiva
    forma.classList.toggle('cont_crear_new_active');
}


function add_to_list(event) {

    // const day = document.getElementById('dateContainer').children[0].value;
    // const month = document.getElementById('monthContainer').children[0].value;
    // const year = document.getElementById('yearContainer').children[0].value;
    
    const deadLine = new Date(document.getElementById('deadline').value);
    
    // POdatke iz elemenata forme prebacujemo u promenljive da bismo ih kasnije iskoristili
    const elems = {
        action: document.getElementById('action_select'),
        title: document.querySelector('.input_title_desc'),
        date: deadLine,
        desc: document.querySelector('.input_description')
    }

    if (!isValid(elems)) {
        return false;
    }

   createListItem({
       action: elems.action.value,
       title: elems.title.value,
       date: elems.date,
       desc: elems.desc.value  
   }); 
    
}


function createListItem( itemData ) {
    // Kreiramo node za postojeci html element u koji cemo dodavati nove elemente (planove za uraditi)
    const ul = document.querySelector('.cont_princ_lists > ul');

    // Ovde cuvamo koliko child nodova (li elemenata trenutno ima u listi)
    const childNum = ul.children.length;

    // Krecemo sa kreiranjem novog child noda koji se dodaje prilikom jednoj izvrsavanja ove funkcije

    // Kreiramo osnovni child node element
    const li = document.createElement('li');
    // i dodajemo mu css klasu
    li.classList.add('list_'+ itemData.action.toLowerCase());

    // dodajemo mu jos jednu klasu koja sadrzi redni broj elementa
    // Pomocu ove klase mozemo jedinstveno identifikovati element
    const itemClass = 'li_num_0_' + (childNum + 1);
    li.classList.add( itemClass );

    //Make li clickable
    li.addEventListener('click', function() {
        //uzeti podatke od servera o trayenom elementu
        fetch('https://my-json-server.typicode.com/nebojsazr/todo_service/todos/' + itemData.id)
            .then((resp) => resp.json())
            .then(function(data) {
                
                //ubaciti podatke u html DOM
                document.getElementById('detailesTitle').innerHTML = data.title;
                document.getElementById('detailesDate').innerHTML = data.date;
                document.getElementById('detailesDescription').innerHTML = data.desc;
            })
            .catch();
        
        
        //otvoriti dijalog 
       // $( "#itemDetailes" ).dialog('open'); Nema potrebe ya ovim, otvoren dijalog kod ajaxa
    });

    // Create header
    const itemHeader = document.createElement('div');
    itemHeader.classList.add('item-header');
    (itemData.date.getTime() <= $.now()) 
        ? itemHeader.classList.add('to-late') 
        : itemHeader.classList.add('stil-hope');
    (itemData.date.getTime() )
    li.appendChild(itemHeader);

    // Create leftTimeContainer
    const leftTimeContainer = document.createElement('span');
    leftTimeContainer.classList.add('left-time-container');
    let leftTimeContainerText = 'Time left: ';

    setInterval(function leftUntil () { 
        let now = new Date();
        let target = new Date(itemData.date);
    
        let timeLeft = new Date( target.getTime() - now.getTime() );
    
        untilDeadline = {
            years: timeLeft.getUTCFullYear() - 1970,  
            months: timeLeft.getUTCMonth(),
            days: timeLeft.getUTCDate() - 1,
            hours: timeLeft.getUTCHours(),
            minutes: timeLeft.getUTCMinutes(),
            seconds: timeLeft.getUTCSeconds(),
        }
    
        leftTimeContainer.innerHTML= `${leftTimeContainerText}
                                    ${untilDeadline.months} meseci
                                    ${untilDeadline.days} dana
                                    ${untilDeadline.hours} sati
                                    ${untilDeadline.minutes} minuta
                                    ${untilDeadline.seconds} sekundi`;

        if((untilDeadline.days < 2 && untilDeadline.days >= 1) && untilDeadline.months.value === 0) {
            itemHeader.classList.add('short-time');
        } else if(untilDeadline.days > 2 && untilDeadline.months >= 0) {
            itemHeader.classList.remove('stil-hope');
        }
        
    }, 1);

    itemHeader.appendChild(leftTimeContainer);

    // Kreiramo div koji predstavlja prvu kolonu i sadrzi podatak 'action' iz forme;
    const div1 = document.createElement('div');
    div1.className = 'col_md_1_list';          // Dodajemo mu njegovu klasu
    div1.innerHTML = `<p>${itemData.action}</p>`;     // i dodajemo mu njegov sadržaj

    // Kreiramo div koji predstavlja drugu kolonu i sadrzi podatke 'title' i 'description' iz forme;
    const div2 = document.createElement('div');
    div2.className = 'col_md_2_list';             // dodajemo mu njegovu klasu

    // Kreiramo pod element title kao node da bismo mogli da operisemo sa njim
    const div2Title = document.createElement('h4');
    div2Title.textContent = itemData.title;
    
    // Kreiramo pod element za description kao node da bismo mogli da operisemo sa njim
    const div2Desc = document.createElement('p');
    div2Desc.classList.add(`desc${childNum+1}`);    // Dodajemo mu jedinstvenu klasu koristeci redni broj
    (itemData.desc.length > 40) 
        ? div2Desc.textContent = itemData.desc.slice(0, 40) + '...'
        : div2Desc.textContent = itemData.desc;

    //div2Desc.textContent = itemData.desc,slice(0, 40);                   // i dodajemo mu text content

    // Dodajemo novokreirane podelemente u drugu kolonu
    div2.appendChild(div2Title);
    div2.appendChild(div2Desc);

    // Kreiramo event listener za click event nad elementom koji predstavlja title
    div2Title.addEventListener('click', function() {
        let desc  = document.getElementsByClassName('desc' + (childNum + 1))[0];  // Selektujemo description element sa odredjenim rednim brojem
        desc.classList.toggle('hidden');                                          // Sakrivamo descritopn element koristeci css classu hidden
    });

    // Kreiramo div koji predstavlja trecu kolonu i sadrzi podatak 'date' iz forme;
    const div3 = document.createElement('div');
    // idodajemo mu njegovu css klasu
    div3.className = 'col_md_3_list';
    div3.innerHTML = '<div class="cont_text_date"><p>' + itemData.date.toLocaleDateString() + '</p></div>';

    // Kreiramo button elemenent koji ce se koristiti za brisanje tekuceg child elementa od liste 
    const deleteBtn = document.createElement('button');
    deleteBtn.innerHTML = '<i class="material-icons"></i>'; // i dodajemo mu text
    deleteBtn.classList.add('cont_btns_options');
    deleteBtn.style.backgroundColor = '#dddad1';
    deleteBtn.classList.add('position');         // i dodajemo mu text

    // nad kreiranim buttonnom kreiramo event listener koji brise tekuci li element iz liste
    deleteBtn.addEventListener('click', function () {
        if(confirm('Da li ste sigurni da zelite da obrisete ovo')) {
            ul.removeChild(li); // brisanje itema
        }
    });
    
    // Dodajemo sve kreirane segmente u glavni li child node
    li.appendChild(div1);
    li.appendChild(div2);
    li.appendChild(div3);
    itemHeader.appendChild(deleteBtn);

    // I konacno dodati kreirani node u document tako da se on pojavljuje u ovom trenutku
    ul.appendChild(li);
    
    // Nakon sto je novi element kreiran i dodan, imalo bi smisla da se elementi forme resetuju
    // Stoga postavljamo njihove vrednosti na pocetno stanje
    document.getElementById('action_select').value = 'SHOPPING';
    document.querySelector('.input_title_desc').value = '';
    //document.querySelector('#dateContainer').value = document.querySelector('#dateContainer > select').children[0].value;
    // document.querySelector('#monthContainer > select').value = document.querySelector('#monthContainer > select').children[0].value;
    // document.querySelector('#yearContainer > select').value = document.querySelector('#yearContainer > select').children[0].value;
    // document.getElementById('date_select').value = 'TODAY';
    document.querySelector('.input_description').value = '';

}

function isValid(elems) {

    elems.title.classList.remove('invalid');
    elems.desc.classList.remove('invalid');
    

    let result = true;
    if( String(elems.title.value).replace(' ', '') == '') {
        // Uokviriti input
        elems.title.classList.add('invalid');
        result = false;
    }

    if( String(elems.desc.value).trim() == '') {
        // Uokviriti input
        elems.desc.classList.add('invalid');
        result = false;
    }

    return result
}
